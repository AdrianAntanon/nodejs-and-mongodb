const MongoClient = require('mongodb').MongoClient;

const uri = "mongodb+srv://Adri:Test1234@adricluster.bykul.mongodb.net/products_test?retryWrites=true&w=majority";

const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const createProduct = async (req, res, next) => {
  const newProduct = {
    name: req.body.name,
    price: req.body.price
  };

  try {
    await client.connect();
    const db = client.db();
    db.collection('products').insertOne(newProduct);
  } catch (error) {
    return res.json({ message: 'Could not store data.' });
  }
  client.close(); //Simpre cerramos la conexión después de conectarnos.

  res.json(newProduct);
};

const getProducts = async (req, res, next) => {
  let products;
  try {
    await client.connect();
    const db = client.db();
    products = await db.collection('products').find().toArray();

  } catch (error) {
    return res.json({ message: 'Could not retrieve products.' });
  }
  client.close(); //Si cierro la conexión evidentemente no puedo modificar la base de datos

  res.json(products);
};

exports.createProduct = createProduct;
exports.getProducts = getProducts;